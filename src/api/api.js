import { db } from "../main";
// import firebase from "firebase";

export default {
  loadData: (url, uid) => {
    return new Promise((resolve, reject) => {
      db.collection(url)
        .where("uid", "==", uid)
        .get()
        .then((querySnapshot) => {
          const data = [];
          if (!querySnapshot.empty) {
            querySnapshot.forEach((doc) => {
              let item = doc.data();
              item.id = doc.id;
              data.push(item);
            });
          }
          resolve(data);
        })
        .catch((error) => {
          console.log("Error getting documents: ", error);
          reject(error);
        });
    });
  },
};
