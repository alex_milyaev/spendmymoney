import { createStore } from "vuex";
import _ from "lodash";
import { db } from "../main";
import api from "../api/api";

export default createStore({
  state: {
    user: null,
    groups: null,
    categories: null,
    transactions: null,
  },
  getters: {
    GET_TRANSACTIONS(state) {
      let transactions = null;
      if (state.transactions) {
        transactions = _.cloneDeep(state.transactions);
        transactions.forEach((transaction) => {
          transaction.user = state.user;
          transaction.group = state.groups.find(
            (group) => group.groupId === transaction.groupId
          );
          transaction.category = state.categories.find(
            (category) => category.categoryId === transaction.categoryId
          );
        });
      }
      return transactions;
    },
  },
  mutations: {
    SET_USER(state, payload) {
      state.user = payload;
      console.log("user", state.user);
    },
    SET_GROUPS(state, payload) {
      state.groups = payload;
      console.log("state.groups", state.groups);
    },
    SET_CATEGORIES(state, payload) {
      state.categories = payload;
      console.log("categories", state.categories);
    },
    SET_TRANSACTIONS(state, payload) {
      state.transactions = payload;
      console.log("transactions", state.transactions);
    },
  },
  actions: {
    EDIT_USER({ commit, state }, editedUser) {
      api
        .loadData("users", state.user.uid)
        .then((user) => {
          console.log("user! ", user);
          db.collection("users")
            .doc(user[0].id)
            .update(editedUser)
            .then(() => {
              console.log("Document successfully updated!");
              commit("SET_USER", user);
            })
            .catch((error) => {
              console.error("Error updating document: ", error);
            });
        })
        .catch((error) => {
          console.log("Error getting documents: ", error);
        });
    },

    LOAD_GROUPS({ commit, state }) {
      api
        .loadData("groups", state.user.uid)
        .then((groups) => {
          commit("SET_GROUPS", groups);
        })
        .catch((error) => {
          console.log("Error getting documents: ", error);
        });
    },
    LOAD_CATEGORIES({ commit, state }) {
      api
        .loadData("categories", state.user.uid)
        .then((categories) => {
          commit("SET_CATEGORIES", categories);
        })
        .catch((error) => {
          console.log("Error getting documents: ", error);
        });
    },
    LOAD_TRANSACTIONS({ commit, state }) {
      api
        .loadData("transactions", state.user.uid)
        .then((transactions) => {
          commit("SET_TRANSACTIONS", transactions);
        })
        .catch((error) => {
          console.log("Error getting documents: ", error);
        });
    },
  },
  modules: {},
});
