import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

// UI Primevue
import PrimeVue from "primevue/config";
import Button from "primevue/button";
import InputMask from "primevue/inputmask";
import InputNumber from "primevue/inputnumber";
import InputSwitch from "primevue/inputswitch";
import InputText from "primevue/inputtext";
import SelectButton from "primevue/selectbutton";
import Calendar from "primevue/calendar";
import Dropdown from "primevue/dropdown";
import DataTable from "primevue/datatable";
import Column from "primevue/column";
import Chart from "primevue/chart";
import Dialog from "primevue/dialog";
import Panel from "primevue/panel";
import ScrollPanel from "primevue/scrollpanel";
import ColorPicker from "primevue/colorpicker";
import Skeleton from "primevue/skeleton";

import "primevue/resources/themes/saga-blue/theme.css";
// import 'primevue/resources/themes/arya-green/theme.css'
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";

const app = createApp(App);

// Firebase
import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyBLeQ9hlzpxnbyD-ZEvrMKoeCEAcaNbcRI",
  authDomain: "spendmoney-8f3c6.firebaseapp.com",
  databaseURL: "https://spendmoney-8f3c6-default-rtdb.firebaseio.com",
  projectId: "spendmoney-8f3c6",
  storageBucket: "spendmoney-8f3c6.appspot.com",
  messagingSenderId: "532908187154",
  appId: "1:532908187154:web:dfc81d4a52c4c03f68c2cb",
});

export const db = firebaseApp.firestore();
export const storage = firebase.storage();
export const storageRef = storage.ref();

// Layout
import Default from "./layouts/Default.vue";
app.component("default-layout", Default);
import Dashboard from "./layouts/Dashboard.vue";
app.component("dashboard-layout", Dashboard);

// Styles
import "./styles/layouts/layout.scss";

// Add plugins
app.use(store);
app.use(router);
app.use(PrimeVue);

// Add UI components
app.component("Button", Button);
app.component("InputMask", InputMask);
app.component("InputNumber", InputNumber);
app.component("InputSwitch", InputSwitch);
app.component("InputText", InputText);
app.component("SelectButton", SelectButton);
app.component("Calendar", Calendar);
app.component("Dropdown", Dropdown);
app.component("DataTable", DataTable);
app.component("Column", Column);
app.component("Chart", Chart);
app.component("Dialog", Dialog);
app.component("Panel", Panel);
app.component("ScrollPanel", ScrollPanel);
app.component("ColorPicker", ColorPicker);
app.component("Skeleton", Skeleton);

app.mount("#app");
