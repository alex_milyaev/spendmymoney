import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "Login",
    component: () => import("../views/Login.vue"),
  },
  {
    path: "/dashboard",
    name: "dashboard",
    meta: { layout: "dashboard", requiresAuth: true },
    component: () => import("../views/Dashboard.vue"),
  },
  {
    path: "/account",
    name: "account",
    meta: { layout: "dashboard", requiresAuth: true },
    component: () => import("../views/Account.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
