const categoriesDefault = [
  { name: "Food", color: "2d56b5" },
  { name: "Сlothes", color: "9c2eb0" },
  { name: "Entertainment", color: "128082" },
  { name: "Transport", color: "2c855e" },
  { name: "Connection", color: "9c5224" },
];

export default categoriesDefault;
